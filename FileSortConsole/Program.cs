﻿using FileSorting;
using System;

namespace FileSortConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //string inFileName = "test.txt";
            //string rezFileName = "rez.txt";

            Console.Write("Введите имя генерируемого файла: ");
            string inFileName = Console.ReadLine();

            Console.Write("Введите размер генерируемого файла: ");
            long fileSize = Convert.ToInt64(Console.ReadLine());

            Console.WriteLine("Генерация файла...");
            DateTime dateBegin = DateTime.Now;
            FileGenerator generator = new FileGenerator();
            generator.GenerateFile(inFileName, fileSize);
            DateTime dateEnd = DateTime.Now;
            TimeSpan interval = dateEnd - dateBegin;
            Console.WriteLine("Время генерации файла - {0:f1} с.", interval.TotalSeconds);

            Console.Write("Введите имя сортируемого файла: ");
            string rezFileName = Console.ReadLine();

            Console.WriteLine("Сортировка файла...");
            dateBegin = DateTime.Now;
            FileSort fileSort = new FileSort();
            fileSort.Sort(inFileName, rezFileName);
            dateEnd = DateTime.Now;
            interval = dateEnd - dateBegin;
            Console.WriteLine("Файл отсортирован!");
            Console.WriteLine("Время сортировки файла - {0:f1} с.", interval.TotalSeconds);
        }
    }
}
