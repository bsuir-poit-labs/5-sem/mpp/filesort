﻿namespace FileSortGUI
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Panel pnlGenerate;
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lbGenerateError = new System.Windows.Forms.Label();
            this.tbFileSize = new System.Windows.Forms.TextBox();
            this.btnSaveDialog = new System.Windows.Forms.Button();
            this.tbSearchSave = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.lbPnlGenerateName = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbSortError = new System.Windows.Forms.Label();
            this.btnSort = new System.Windows.Forms.Button();
            this.btnOpenDialog = new System.Windows.Forms.Button();
            this.tbSearchOpen = new System.Windows.Forms.TextBox();
            this.lbPnlSortName = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            pnlGenerate = new System.Windows.Forms.Panel();
            pnlGenerate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlGenerate
            // 
            pnlGenerate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            pnlGenerate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pnlGenerate.Controls.Add(this.btnGenerate);
            pnlGenerate.Controls.Add(this.lbGenerateError);
            pnlGenerate.Controls.Add(this.tbFileSize);
            pnlGenerate.Controls.Add(this.btnSaveDialog);
            pnlGenerate.Controls.Add(this.tbSearchSave);
            pnlGenerate.Location = new System.Drawing.Point(12, 21);
            pnlGenerate.Name = "pnlGenerate";
            pnlGenerate.Size = new System.Drawing.Size(560, 167);
            pnlGenerate.TabIndex = 0;
            pnlGenerate.Tag = "";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(15, 117);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(178, 29);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "Сгенерировать файл";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.BtnGenerate_Click);
            // 
            // lbGenerateError
            // 
            this.lbGenerateError.AutoSize = true;
            this.lbGenerateError.ForeColor = System.Drawing.Color.Maroon;
            this.lbGenerateError.Location = new System.Drawing.Point(199, 121);
            this.lbGenerateError.Name = "lbGenerateError";
            this.lbGenerateError.Size = new System.Drawing.Size(0, 20);
            this.lbGenerateError.TabIndex = 3;
            // 
            // tbFileSize
            // 
            this.tbFileSize.Location = new System.Drawing.Point(15, 74);
            this.tbFileSize.Name = "tbFileSize";
            this.tbFileSize.PlaceholderText = "Размер файла (б)";
            this.tbFileSize.Size = new System.Drawing.Size(131, 27);
            this.tbFileSize.TabIndex = 2;
            // 
            // btnSaveDialog
            // 
            this.btnSaveDialog.Location = new System.Drawing.Point(452, 25);
            this.btnSaveDialog.Name = "btnSaveDialog";
            this.btnSaveDialog.Size = new System.Drawing.Size(94, 29);
            this.btnSaveDialog.TabIndex = 1;
            this.btnSaveDialog.Text = "Обзор..";
            this.btnSaveDialog.UseVisualStyleBackColor = true;
            this.btnSaveDialog.Click += new System.EventHandler(this.BtnSaveDialog_Click);
            // 
            // tbSearchSave
            // 
            this.tbSearchSave.Location = new System.Drawing.Point(15, 27);
            this.tbSearchSave.Name = "tbSearchSave";
            this.tbSearchSave.PlaceholderText = "Путь к генерируему файлу";
            this.tbSearchSave.Size = new System.Drawing.Size(431, 27);
            this.tbSearchSave.TabIndex = 0;
            // 
            // lbPnlGenerateName
            // 
            this.lbPnlGenerateName.AutoSize = true;
            this.lbPnlGenerateName.Location = new System.Drawing.Point(28, 11);
            this.lbPnlGenerateName.Name = "lbPnlGenerateName";
            this.lbPnlGenerateName.Size = new System.Drawing.Size(131, 20);
            this.lbPnlGenerateName.TabIndex = 1;
            this.lbPnlGenerateName.Text = "Генерация файла";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            this.saveFileDialog.InitialDirectory = "D:\\Study\\Programs\\5_Sem\\SPP\\FileSort\\FileSortGUI\\bin\\Debug\\netcoreapp3.1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbSortError);
            this.panel1.Controls.Add(this.btnSort);
            this.panel1.Controls.Add(this.btnOpenDialog);
            this.panel1.Controls.Add(this.tbSearchOpen);
            this.panel1.Location = new System.Drawing.Point(12, 212);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(560, 124);
            this.panel1.TabIndex = 2;
            // 
            // lbSortError
            // 
            this.lbSortError.AutoSize = true;
            this.lbSortError.ForeColor = System.Drawing.Color.Maroon;
            this.lbSortError.Location = new System.Drawing.Point(172, 78);
            this.lbSortError.Name = "lbSortError";
            this.lbSortError.Size = new System.Drawing.Size(0, 20);
            this.lbSortError.TabIndex = 3;
            // 
            // btnSort
            // 
            this.btnSort.Location = new System.Drawing.Point(15, 74);
            this.btnSort.Name = "btnSort";
            this.btnSort.Size = new System.Drawing.Size(151, 29);
            this.btnSort.TabIndex = 2;
            this.btnSort.Text = "Сортировать файл";
            this.btnSort.UseVisualStyleBackColor = true;
            this.btnSort.Click += new System.EventHandler(this.BtnSort_Click);
            // 
            // btnOpenDialog
            // 
            this.btnOpenDialog.Location = new System.Drawing.Point(452, 25);
            this.btnOpenDialog.Name = "btnOpenDialog";
            this.btnOpenDialog.Size = new System.Drawing.Size(94, 29);
            this.btnOpenDialog.TabIndex = 1;
            this.btnOpenDialog.Text = "Обзор..";
            this.btnOpenDialog.UseVisualStyleBackColor = true;
            this.btnOpenDialog.Click += new System.EventHandler(this.BtnOpenDialog_Click);
            // 
            // tbSearchOpen
            // 
            this.tbSearchOpen.Location = new System.Drawing.Point(15, 27);
            this.tbSearchOpen.Name = "tbSearchOpen";
            this.tbSearchOpen.PlaceholderText = "Путь к сортируемому файлу";
            this.tbSearchOpen.Size = new System.Drawing.Size(431, 27);
            this.tbSearchOpen.TabIndex = 0;
            // 
            // lbPnlSortName
            // 
            this.lbPnlSortName.AutoSize = true;
            this.lbPnlSortName.Location = new System.Drawing.Point(28, 200);
            this.lbPnlSortName.Name = "lbPnlSortName";
            this.lbPnlSortName.Size = new System.Drawing.Size(139, 20);
            this.lbPnlSortName.TabIndex = 3;
            this.lbPnlSortName.Text = "Сортировка файла";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            this.openFileDialog.InitialDirectory = "D:\\Study\\Programs\\5_Sem\\SPP\\FileSort\\FileSortGUI\\bin\\Debug\\netcoreapp3.1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(587, 356);
            this.Controls.Add(this.lbPnlSortName);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbPnlGenerateName);
            this.Controls.Add(pnlGenerate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(14, 14);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сортировка большого файла";
            pnlGenerate.ResumeLayout(false);
            pnlGenerate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Panel pnlGenerate;
        private System.Windows.Forms.TextBox tbSearchSave;
        private System.Windows.Forms.Label lbPnlGenerateName;
        private System.Windows.Forms.Button btnSaveDialog;
        private System.Windows.Forms.TextBox tbFileSize;
        private System.Windows.Forms.Label lbGenerateError;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnOpenDialog;
        private System.Windows.Forms.TextBox tbSearchOpen;
        private System.Windows.Forms.Label lbPnlSortName;
        private System.Windows.Forms.Button btnSort;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lbSortError;
    }
}

