﻿using FileSorting;
using System;
using System.Windows.Forms;

namespace FileSortGUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void BtnSaveDialog_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            tbSearchSave.Text = saveFileDialog.FileName;
        }

        private void BtnOpenDialog_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            tbSearchOpen.Text = openFileDialog.FileName;
        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            lbGenerateError.Text = "";
            if (String.IsNullOrEmpty(tbSearchSave.Text))
            {
                lbGenerateError.Text = "Выберете файл! ";
                return;
            }
            if (String.IsNullOrEmpty(tbFileSize.Text))
            {
                lbGenerateError.Text = "Укажите размер файла!";
                return;
            }
            if (!int.TryParse(tbFileSize.Text, out int fileSize))
            {
                lbGenerateError.Text = "Некорректный ввод размера файла!";
                return;
            }

            DateTime dateBegin = DateTime.Now;
            FileGenerator generator = new FileGenerator();
            generator.GenerateFile(tbSearchSave.Text.Trim(), fileSize);
            DateTime dateEnd = DateTime.Now;
            TimeSpan interval = dateEnd - dateBegin;
            MessageBox.Show("Время генерации файла - " +
                Math.Round(interval.TotalSeconds, 2).ToString() + " с.");
        }

        private void BtnSort_Click(object sender, EventArgs e)
        {
            lbSortError.Text = "";
            if (String.IsNullOrEmpty(tbSearchOpen.Text))
            {
                lbGenerateError.Text = "Выберете файл! ";
                return;
            }

            DateTime dateBegin = DateTime.Now;
            FileSort fileSort = new FileSort();
            fileSort.Sort(tbSearchOpen.Text.Trim(), "rez.txt");
            DateTime dateEnd = DateTime.Now;
            TimeSpan interval = dateEnd - dateBegin;
            Console.WriteLine("Файл отсортирован!");
            Console.WriteLine("Время сортировки файла - {0:f1} с.", interval.TotalSeconds);

            MessageBox.Show("Файл отсортирован!\n" +
                "Время сортировки файла - " +
                Math.Round(interval.TotalSeconds, 2).ToString() + " с.");
        }
    }
}
