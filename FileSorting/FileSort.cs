﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FileSorting
{
    public class FileSort
    {
        private long BufferSize = 100 * 1024 * 1024; // 100 Mb

        public FileSort()
        {

        }

        public FileSort(long bufferSize)
        {
            this.BufferSize = bufferSize;
        }

        public void Sort(string inFileName, string rezFileName)
        {
            List<string> tempFileNames = MakeTempFiles(inFileName);
            List<StreamReader> readers = new List<StreamReader>(tempFileNames.Count);
            List<string> lines = new List<string>(tempFileNames.Count);
            for (int i = 0; i < tempFileNames.Count; i++)
            {
                StreamReader reader = new StreamReader(tempFileNames[i], System.Text.Encoding.Unicode);
                readers.Add(reader);
                lines.Add(reader.ReadLine());
            }

            using (StreamWriter writer = new StreamWriter(rezFileName, false, System.Text.Encoding.Unicode))
            {
                while (readers.Count != 0)
                {
                    string minLine = lines[0];
                    int minIndex = 0;

                    for (int i = 1; i < lines.Count; i++)
                    {
                        if (String.Compare(lines[i], minLine, StringComparison.Ordinal) < 0)
                        {
                            minLine = lines[i];
                            minIndex = i;
                        }
                    }

                    writer.WriteLine(minLine);
                    if ((lines[minIndex] = readers[minIndex].ReadLine()) == null)
                    {
                        lines.RemoveAt(minIndex);

                        readers[minIndex].Close();
                        readers.RemoveAt(minIndex);

                        File.Delete(tempFileNames[minIndex]);
                        tempFileNames.RemoveAt(minIndex);
                    }
                }
            }
        }

        private List<string> MakeTempFiles(string inFileName)
        {
            List<string> TempFileNames = new List<string>();
            using (StreamReader reader = new StreamReader(inFileName, System.Text.Encoding.Unicode))
            {
                while (!reader.EndOfStream)
                {
                    List<string> lines = new List<string>();
                    string line;
                    long tempFileSize = 0;

                    while ((tempFileSize < BufferSize) && ((line = reader.ReadLine()) != null))
                    {
                        lines.Add(line);
                        tempFileSize += line.Length * 2 + 4;
                    }

                    string[] linesArray = lines.ToArray();
                    Array.Sort(linesArray, StringComparer.Ordinal);
                    string tempFilePath = Path.GetTempFileName();
                    using (StreamWriter writer = new StreamWriter(tempFilePath, false, System.Text.Encoding.Unicode))
                    {
                        foreach (string item in linesArray)
                            writer.WriteLine(item);
                    }
                    TempFileNames.Add(tempFilePath);
                }
            }
            return TempFileNames;
        }
    }
}
