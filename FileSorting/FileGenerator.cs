﻿using System;
using System.IO;

namespace FileSorting
{
    public class FileGenerator
    {
        private readonly char[] Symbols = new char[]
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };
        private const int minWordsCount = 3;
        private const int maxWordsCount = 10;
        private const int minLettersCount = 2;
        private const int maxLettersCount = 20;

        public void GenerateFile(string path, long size)
        {
            using (StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.Unicode))
            {
                Random random = new Random();

                while (size > 0)
                {
                    int wordsCount = random.Next(minWordsCount, maxWordsCount + 1);
                    for (int i = 0; (i < wordsCount) && (size > 0); i++)
                    {
                        int lettersCount = random.Next(minLettersCount, maxLettersCount + 1);
                        for (int j = 0; (j < lettersCount) && (size > 0); j++)
                        {
                            int index = random.Next(0, Symbols.Length);
                            writer.Write(Symbols[index]);
                            size -= 2;
                        }
                        if (size > 0)
                        {
                            writer.Write(" ");
                            size -= 2;
                        }
                    }
                    if (size > 0)
                    {
                        writer.WriteLine();
                        size -= 4;
                    }
                }
            }
        }
    }
}
